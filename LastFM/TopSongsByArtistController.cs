﻿using System;

using UIKit;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace LastFM
{
	public partial class TopSongsByArtistController : UIViewController
	{
		public Artist Artist {
			get;
			set;
		}

		public UITableView TableView {
			get;
			set;
		}

		public TopSongsByArtistDataSource TableViewDataSource {
			get;
			set;
		}

		public TopSongsByArtistController (Artist artist) : base ("TopSongsByArtistController", null)
		{
			Artist = artist;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			Title = "Top Tracks for " + Artist.Name;
			NavigationItem.Title = "Top Tracks for " + Artist.Name;

			TableViewDataSource = new TopSongsByArtistDataSource ();

			TableView = new UITableView {
				Frame = UIScreen.MainScreen.Bounds,
				DataSource = TableViewDataSource
			};

			TableView.Scrolled += (object sender, EventArgs e) => {
				UIScrollView scrollView = sender as UIScrollView;
				float scrollViewHeight = (float)scrollView.Frame.Size.Height;
				float scrollContentSizeHeight = (float)scrollView.ContentSize.Height;
				float scrollOffset = (float)scrollView.ContentOffset.Y;

				if (scrollOffset == 0)
				{
					// then we are at the top
				}
				else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight * .80)
				{
					if(TableViewDataSource.Songs != null &&
						!TableViewDataSource.IsFetching)
					{
						TableViewDataSource.IsFetching = true;
						TableView.ReloadData();

						Task.Run(async() => {
							await TableViewDataSource.LoadMore();
						})
						.ContinueWith(t => 
						{
							TableView.ReloadData();
							TableViewDataSource.IsFetching = false;	
						}, TaskScheduler.FromCurrentSynchronizationContext());	
					}				
				}
			};

			View.AddSubview (TableView);

			Task.Run(async() => {
				await TableViewDataSource.Reset (Artist);
			})
			.ContinueWith(t => 
			{
				TableView.ReloadData();
			}, TaskScheduler.FromCurrentSynchronizationContext());	
			
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}


