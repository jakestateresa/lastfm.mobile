﻿using System;
using System.Threading.Tasks;
using System.Json;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace LastFM
{
	public static class LastFMService
	{
		public const string API_KEY = "824d5f76b5909140fa7eb85ee8c9ef42";

		public static async Task<byte[]> LoadImage (string imageUrl)
		{
			var httpClient = new HttpClient();

			Task<byte[]> contentsTask = httpClient.GetByteArrayAsync (imageUrl);

			// await! control returns to the caller and the task continues to run on another thread
			var contents = await contentsTask;

			// load from bytes
			return contents;
		}

		public static async Task<IEnumerable<Artist>> FetchArtists (string country, int page = 1, int limit=30)
		{
			var geoGetTopArtistsUrl = "http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=" + country + "&api_key=" + API_KEY+ "&page=" + page + "&limit=" + limit + "&format=json";
			var request = (HttpWebRequest)HttpWebRequest.Create (new Uri (geoGetTopArtistsUrl));
			request.ContentType = "application/json";
			request.Method = "GET";

			// Send the request to the server and wait for the response:
			using (WebResponse response = await request.GetResponseAsync ())
			{
				// Get a stream representation of the HTTP web response:
				using (Stream stream = response.GetResponseStream ())
				{
					// Use this stream to build a JSON document object:
					return await Task.Run ( async() => {
						JsonValue jsonDoc = JsonObject.Load (stream);

						if(!jsonDoc.ContainsKey("error")) {
							var topArtistContainer = jsonDoc["topartists"];
							var artistContainer = topArtistContainer["artist"];

							var parsedArtists = new List<Artist>();
							foreach (JsonValue artist in artistContainer) {
								var mediumImage = ((JsonArray)artist["image"]).Where(x => x["size"] == "medium").First();
								parsedArtists.Add(new Artist {
									Name = artist["name"],
									Image = await LastFMService.LoadImage (mediumImage["#text"])					
								});
							}
							return parsedArtists;
						}
						return new List<Artist>();
					});
				}
			}
		}

		public static async Task<IEnumerable<Song>> FetchTopSongs (string artist, int page = 1, int limit=30)
		{
			var getTopTracksUrl = "http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=" + artist + "&api_key=" + API_KEY+ "&page=" + page + "&limit=" + limit + "&format=json";
			var request = (HttpWebRequest)HttpWebRequest.Create (new Uri (getTopTracksUrl));
			request.ContentType = "application/json";
			request.Method = "GET";

			// Send the request to the server and wait for the response:
			using (WebResponse response = await request.GetResponseAsync ())
			{
				// Get a stream representation of the HTTP web response:
				using (Stream stream = response.GetResponseStream ())
				{
					// Use this stream to build a JSON document object:
					return await Task.Run ( async() => {
						JsonValue jsonDoc = JsonObject.Load (stream);

						if(!jsonDoc.ContainsKey("error")) {
							var topTracks = jsonDoc["toptracks"];
							var tracks = topTracks["track"];

							var parsedSongs = new List<Song>();
							foreach (JsonValue track in tracks) {
								var mediumImage = ((JsonArray)track["image"]).Where(x => x["size"] == "medium").First();
								parsedSongs.Add(new Song {
									Title = track["name"],
									Image = await LastFMService.LoadImage (mediumImage["#text"])					
								});
							}
							return parsedSongs;
						}
						return new List<Song>();
					});
				}
			}
		}
	}
}

