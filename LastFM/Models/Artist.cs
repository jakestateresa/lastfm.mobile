﻿using System;
using System.Threading.Tasks;

namespace LastFM
{
	public class Artist
	{
		public string Name {
			get;
			set;
		}

		public byte[] Image {
			get;
			set;
		}
	}
}

