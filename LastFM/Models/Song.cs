﻿using System;

namespace LastFM
{
	public class Song
	{
		public string Title {
			get;
			set;
		}

		public byte[] Image {
			get;
			set;
		}
	}
}

