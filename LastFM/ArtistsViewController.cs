﻿using System;

using UIKit;
using System.Drawing;
using System.Reactive.Linq;
using System.Reactive;
using System.Threading;
using System.Threading.Tasks;
using Foundation;

namespace LastFM
{
	public partial class ArtistsViewController : UIViewController
	{
		public UISearchController SearchController {
			get;
			set;
		}

		public UITableView TableView {
			get;
			set;
		}

		public ArtistsViewDataSource TableViewDataSource {
			get;
			set;
		}

		public ArtistsViewController () : base ("ArtistsViewController", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			Title = "LastFM";
			NavigationItem.Title = "LastFM";

			SearchController = new UISearchController ((UIViewController)null);
			SearchController.DimsBackgroundDuringPresentation = false;
			DefinesPresentationContext = true;

			TableViewDataSource = new ArtistsViewDataSource ();
			TableViewDataSource.Owner = this;

			TableView = new UITableView {
				Frame = UIScreen.MainScreen.Bounds,
				TableHeaderView = SearchController.SearchBar,
				Source = TableViewDataSource
			};

			View.AddSubview (TableView);

			var queryTextChangedObservable = Observable.FromEventPattern<EventHandler<UISearchBarTextChangedEventArgs>, UISearchBarTextChangedEventArgs>
				(s => SearchController.SearchBar.TextChanged += s, s => SearchController.SearchBar.TextChanged -= s);

			queryTextChangedObservable.Throttle(TimeSpan.FromMilliseconds(350))
				.Scan(new {cts = new CancellationTokenSource(), e = default(EventPattern<UISearchBarTextChangedEventArgs>)},
					(previous, newObj) => { previous.cts.Cancel();
						return new {cts = new CancellationTokenSource(), e = newObj};
					})
				.ObserveOn(SynchronizationContext.Current)
				.Subscribe(
					async x => { 
						var country = ((UISearchBar)x.e.Sender).Text;
						if(!TableViewDataSource.IsFetching)
						{
							TableViewDataSource.IsFetching = true;
							TableView.ReloadData();

							await TableViewDataSource.Reset(country);

							TableView.ReloadData();
							TableViewDataSource.IsFetching = false;	
						}		
					});

			// Perform any additional setup after loading the view, typically from a nib.
		}
			
		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}	

	}
}


