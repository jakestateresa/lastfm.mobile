﻿using System;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using System.Threading.Tasks;

namespace LastFM
{
	public class ArtistsViewDataSource : UITableViewSource
	{
		public string Country {
			get;
			set;
		}

		public UIViewController Owner {
			get;
			set;
		}

		public List<Artist> Artists {
			get;
			set;
		}

		public int CurrentPage {
			get;
			set;
		}

		public bool IsFetching {
			get;
			set;
		}

		string CellIdentifier = "ArtistCell";

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			var rowCount = 0;

			if (Artists != null) 
			{
				rowCount = Artists.Count ();
			}

			if (IsFetching) 
			{
				rowCount += 1;
			}

			return rowCount;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);

			if (cell == null)
			{ 
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier); 
				cell.ImageView.Image = null;
				cell.Accessory = UITableViewCellAccessory.None;
			}

			if (Artists != null && Artists.Count() > 0)				
			{
				if (Artists.Count () != indexPath.Row) {
					var artist = Artists.ElementAt (indexPath.Row);
					cell.TextLabel.Text = artist.Name;
					cell.ImageView.Image = UIImage.LoadFromData (NSData.FromArray (artist.Image));
					cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				}
				else 
				{
					cell.TextLabel.Text = "Loading...";
					cell.ImageView.Image = null;
					cell.Accessory = UITableViewCellAccessory.None;
				}
			}
			else 
			{
				cell.TextLabel.Text = "Loading...";
				cell.ImageView.Image = null;
				cell.Accessory = UITableViewCellAccessory.None;
			}

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var artist = Artists[indexPath.Row];
			Owner.NavigationController.PushViewController (new TopSongsByArtistController (artist),true);
			tableView.DeselectRow (indexPath, false);
		}

		public override void Scrolled (UIScrollView scrollView)
		{
			float scrollViewHeight = (float)scrollView.Frame.Size.Height;
			float scrollContentSizeHeight = (float)scrollView.ContentSize.Height;
			float scrollOffset = (float)scrollView.ContentOffset.Y;

			if (scrollOffset == 0)
			{
				// then we are at the top
			}
			else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight * .80)
			{
				if(Artists != null &&
					!IsFetching)
				{
					IsFetching = true;
					((ArtistsViewController)Owner).TableView.ReloadData();

					Task.Run(async() => {
						await LoadMore();
					})
					.ContinueWith(t => 
					{
						((ArtistsViewController)Owner).TableView.ReloadData();
						IsFetching = false;	
					}, TaskScheduler.FromCurrentSynchronizationContext());	
				}				
			}
		}

		public async Task LoadMore()
		{
			this.CurrentPage++;
			var nextPage = await LastFMService.FetchArtists (Country, CurrentPage, 15);
			Artists.AddRange (nextPage);
		}

		public async Task Reset(String country) 
		{
			Country = country;
			Artists = new List<Artist>();
			CurrentPage = 0;
			await LoadMore ();
		}
	}
}

