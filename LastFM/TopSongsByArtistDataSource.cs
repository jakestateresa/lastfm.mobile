﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using System.Linq;

namespace LastFM
{
	public class TopSongsByArtistDataSource : UITableViewDataSource
	{
		public Artist Artist {
			get;
			set;
		}

		public List<Song> Songs {
			get;
			set;
		}

		public int CurrentPage {
			get;
			set;
		}

		public bool IsFetching {
			get;
			set;
		}

		string CellIdentifier = "ArtistCell";

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			var rowCount = 0;

			if (Songs != null) 
			{
				rowCount = Songs.Count();
			}

			if (IsFetching) 
			{
				rowCount += 1;
			}

			return rowCount;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);

			if (cell == null)
			{ 
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier); 
				cell.ImageView.Image = null;
				cell.Accessory = UITableViewCellAccessory.None;
			}

			if (Songs != null && Songs.Count() > 0)				
			{
				if (Songs.Count () != indexPath.Row) {
					var song = Songs.ElementAt (indexPath.Row);
					cell.TextLabel.Text = song.Title;
					cell.ImageView.Image = UIImage.LoadFromData (NSData.FromArray (song.Image));
				}
				else 
				{
					cell.TextLabel.Text = "Loading...";
					cell.ImageView.Image = null;
				}
			}
			else 
			{
				cell.TextLabel.Text = "Loading...";
				cell.ImageView.Image = null;
			}

			return cell;
		}

		public async Task LoadMore()
		{
			this.CurrentPage++;
			var nextPage = await LastFMService.FetchTopSongs (Artist.Name, CurrentPage, 15);
			Songs.AddRange (nextPage);
		}

		public async Task Reset(Artist artist) 
		{
			Artist = artist;
			Songs = new List<Song>();
			CurrentPage = 0;
			await LoadMore ();
		}
	}
}

