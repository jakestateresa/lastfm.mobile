# Overview #

This repository contains the source code for a mobile application created using Xamarin.iOS and C# which exercises the LastFM api to show the top artists for a given country as well as the top hits for an artist. 

### Features ###

 * Allow the user to enter a country on the search bar
 * Shows the name and image of top artists from the given country
 * Loads the next next top artists when the user scrolls towards the bottom of the list
 * Tapping on an row loads the top hits by the selected artist
 * Loads the next next top hits when the user scrolls towards the bottom of the list

### Environment Setup ###

 * Install Xamarin Studio from [https://www.xamarin.com/download](https://www.xamarin.com/download)
 * Download the source code of the application from [downloads page](https://bitbucket.org/jakestateresa/lastfm/downloads) or clone the repository using the command ```git clone https://bitbucket.org/jakestateresa/lastfm.git```
 * Extract the source code suitable directory
 * Double click on the file ```LastFM.sln``` to open the project
 * Press on Cmd+Enter to run the application on the simulator

### Compromises and Shortcuts ###

* The images are loaded immediately. Load times can be improved by providing a default image and then loading the actual image on demand.
* ArtistsViewController and TopSongsByArtistViewController can be refactored to have a common base class which accepts a generic type for the TableViewDataSource and TableView properties
* ArtistsViewDataSource and TopSongsByArtistDataSource can be refactored to have a common base class which accepts a generic type for the SearchTerm and Items collection properties. This base class will also have an overloaded method for Loading the 
* Source code for throttling the search input and cancelling previous requests can be improved.

### Deviations from requirements ###

 * page size is increased from 5 to 15
 * navigating to other pages is provided using infinite scroll
 * list of top tracks per artist contains song image 
 * pagination support on the top tracks per artist page

### Additional Notes ###
 
 * A [video](https://bitbucket.org/jakestateresa/lastfm/raw/047bcba85aeb826df03c1bd03c046041a1634123/LastFM.mov) showing the application in action is available